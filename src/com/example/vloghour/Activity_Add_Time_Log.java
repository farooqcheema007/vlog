package com.example.vloghour;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_Add_Time_Log extends Activity implements OnItemSelectedListener,OnClickListener,OnFocusChangeListener{
	CustomScrollView scoll_view_time_log;
	Button btn_add_time_log_database,btnChangeSupervisorData,btnAddSupervisorData;
	EditText et_volunteer_purpose, et_activity_info , et_descritpion , et_hours , et_start_time , et_end_time,et_supervisor_des,
		et_note_des,et_volunter_organization;
	Spinner sp_select_supervisor;
	DBController controller = new DBController(this);
	Bundle extras;
	String[] supervisorName;
	ArrayAdapter<String> st;
	String valueOfSupervisor;
	TimeEntry obj;
	CustomDateTimePicker custom;
	long secondsStart , secondsEnd;
	boolean distanceStart = false, distanceFinish= false, distanceHoursStart= false, distanceHoursEnd= false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_time_log);
		Registerwidgets();
		extras = getIntent().getExtras();
		if(extras.getInt("value")==1){
			findSupervisorList();
			obj = controller.getVolunteerInfo(extras.getString("Id"));
			resetValue(obj);
		}else{
			findSupervisorList();
		}
		
	}
	
	private void dateAndTimePickerStartTime(){
		 custom = new CustomDateTimePicker(this,
		            new CustomDateTimePicker.ICustomDateTimeListener() {
		                @Override
		                public void onCancel() {

		                }

						@Override
						public void onSet(Dialog dialog,
								Calendar calendarSelected, Date dateSelected,
								int year, String monthFullName,
								String monthShortName, int monthNumber,
								int date, String weekDayFullName,
								String weekDayShortName, int hour24,
								int hour12, int min, int sec, String AM_PM) {
							// TODO Auto-generated method stub
							
							et_start_time .setText(makeDateToString(dateSelected));
//							dateStart = dateSelected;
//							et_start_time .setText(calendarSelected
//                                .get(Calendar.DAY_OF_MONTH)
//                                + "/" + (monthNumber+1) + "/" + year
//                                + ", " + hour12 + ":" + min
//                                + " " + AM_PM);
							secondsStart = dateSelected.getTime();
							if(distanceHoursStart)
								et_hours.setText(String.valueOf(distanceBetweenDate(secondsEnd,secondsStart)));

						}
		            });
		 
		 custom.set24HourFormat(false);
		 /**
		  * Pass Directly current data and time to show when it pop up
		  */
		 custom.setDate(Calendar.getInstance());
	}
	private void dateAndTimePickerEndTime(){
		 custom = new CustomDateTimePicker(this,
		            new CustomDateTimePicker.ICustomDateTimeListener() {
		                @Override
		                public void onCancel() {

		                }

						@Override
						public void onSet(Dialog dialog,
								Calendar calendarSelected, Date dateSelected,
								int year, String monthFullName,
								String monthShortName, int monthNumber,
								int date, String weekDayFullName,
								String weekDayShortName, int hour24,
								int hour12, int min, int sec, String AM_PM) {
							// TODO Auto-generated method stub
							
//							DateFinal = dateSelected;
							et_end_time.setText(makeDateToString(dateSelected));
//							et_end_time.setText(calendarSelected
//                               .get(Calendar.DAY_OF_MONTH)
//                               + "/" + (monthNumber+1) + "/" + year
//                               + ", " + hour12 + ":" + min
//                               + " " + AM_PM);
							secondsEnd = dateSelected.getTime();
							if(distanceHoursEnd)
								et_hours.setText(String.valueOf(distanceBetweenDate(secondsEnd,secondsStart)));
							
								
						}
		            });
		 
		 custom.set24HourFormat(false);
		 /**
		  * Pass Directly current data and time to show when it pop up
		  */
		 custom.setDate(Calendar.getInstance());
	}

	private void Registerwidgets(){
		et_supervisor_des = (EditText)findViewById(R.id.superVisorDescription);
		et_note_des = (EditText)findViewById(R.id.notesDescription);
		btn_add_time_log_database = (Button)findViewById(R.id.btnAddTimeLogDataBase);
		et_volunteer_purpose = (EditText)findViewById(R.id.volunteeringPurposeEditText);
		et_activity_info = (EditText)findViewById(R.id.activityInformationEditText);
//		et_date = (EditText)findViewById(R.id.dateEditText);
		et_descritpion = (EditText)findViewById(R.id.descriptionEditText);
		et_hours = (EditText)findViewById(R.id.hourEditText);
		sp_select_supervisor = (Spinner)findViewById(R.id.selectSuperVisorEditText);
		btnChangeSupervisorData = (Button)findViewById(R.id.btnChangeSupervisorData);
		et_start_time = (EditText)findViewById(R.id.startTimeEditText);
		et_end_time = (EditText)findViewById(R.id.endTimeEditText);
		btnAddSupervisorData = (Button)findViewById(R.id.btnAddSupervisorData);
		et_volunter_organization=(EditText)findViewById(R.id.volunteeringOrganizationEditText);
		scoll_view_time_log = (CustomScrollView)findViewById(R.id.scrollViewTimeLog);
		
		et_end_time.setOnClickListener(this);
		et_start_time.setOnClickListener(this);
		btn_add_time_log_database.setOnClickListener(this);
		sp_select_supervisor.setOnItemSelectedListener(this);
		btnChangeSupervisorData.setOnClickListener(this);
		btnAddSupervisorData.setOnClickListener(this);
		et_supervisor_des.setOnFocusChangeListener(this);
		et_note_des.setOnFocusChangeListener(this);
	}
	
	
//	private void editTextOnFocusChange(){
//		et_start_time.setInputType(InputType.TYPE_NULL);
//		et_end_time.setInputType(InputType.TYPE_NULL);
////		et_start_time.setOnFocusChangeListener(new View.OnFocusChangeListener() {
////	        @Override
////	        public void onFocusChange(View v, boolean focus) {
////	    			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
////	    			imm.hideSoftInputFromWindow(et_start_time.getWindowToken(), 0);			
////	        }
////	    });
////		et_end_time.setOnFocusChangeListener(new View.OnFocusChangeListener() {
////	        @Override
////	        public void onFocusChange(View v, boolean focus) {
////	    			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
////	    			imm.hideSoftInputFromWindow(et_end_time.getWindowToken(), 0);
////	        }
////	    });
//	}
	private void findSupervisorList(){
		ArrayList<Supervisor> supervisorList =  controller.getAllSupervisor();
		supervisorName = new String[supervisorList.size()+1];
		supervisorName[0]="Select Supervisor";
		if(supervisorList.size()!=0) {
			for(int i=0;i<supervisorList.size();i++){
				supervisorName[i+1]=supervisorList.get(i).getName();
			}
		}
		
		st=new ArrayAdapter<String>(Activity_Add_Time_Log.this, android.R.layout.simple_spinner_item,supervisorName);
		st.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_select_supervisor.setSelection(supervisorName.length-1);
		sp_select_supervisor.setAdapter(st);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAddTimeLogDataBase:
			if (et_volunteer_purpose.getText().toString().matches("")) 
			    Toast.makeText(this, "Please Enter Volunteer Purpose..", Toast.LENGTH_SHORT).show();
			else if(et_volunter_organization.getText().toString().matches(""))
				Toast.makeText(this, "Please Enter Volunteer Organization..", Toast.LENGTH_SHORT).show();
			else if(extras.getInt("value")==0)
				addVolunteer();
			else if(extras.getInt("value")==1)
				editVolunteer();
			break;
		case R.id.btnChangeSupervisorData:
			if(sp_select_supervisor.getSelectedItem().toString().equals("Select Supervisor"))
				Toast.makeText(this, "Please Select Supervisor First..", Toast.LENGTH_SHORT).show();
			else{
	 			Intent intent_add_superviosr = new Intent(Activity_Add_Time_Log.this,Activity_Add_Supervisor.class);
				Bundle bundle	=	new Bundle();
				bundle.putString("Id",findSupervisorId(sp_select_supervisor.getSelectedItem().toString())); 
				bundle.putInt("value", 2); 
				intent_add_superviosr.putExtras(bundle);
				startActivity(intent_add_superviosr);
			}
			break;
		case R.id.startTimeEditText:
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(et_start_time.getWindowToken(), 0);			
			dateAndTimePickerStartTime();
			distanceFinish = true;
			if(distanceStart){
				custom.settingMaxDate(secondsEnd);
				distanceHoursStart = true;
				distanceFinish = false;
			}
			custom.showDialog();			 
			break;
		case R.id.endTimeEditText:
			InputMethodManager immm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			immm.hideSoftInputFromWindow(et_end_time.getWindowToken(), 0);			
			dateAndTimePickerEndTime();
			distanceStart = true;
			if(distanceFinish){
				custom.settingMinDate(secondsStart);
				distanceHoursEnd = true;
				distanceStart = false;
			}
			custom.showDialog();
			break;
			
		case R.id.btnAddSupervisorData:
 			Intent intent_add_superviosr_data = new Intent(Activity_Add_Time_Log.this,Activity_Add_Supervisor.class);
 			Bundle bundle_supervisor =	new Bundle();
 			bundle_supervisor.putInt("value", 0); 
			intent_add_superviosr_data.putExtras(bundle_supervisor);
 			startActivity(intent_add_superviosr_data);
 			break;
 			
		case R.id.superVisorDescription:
//			if (hasFocus) {
			InputMethodManager immText=(InputMethodManager)getSystemService(Activity_Add_Time_Log.this.INPUT_METHOD_SERVICE);
			immText.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//			} else {
//				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//				imm.hideSoftInputFromWindow(tv_supervisor_des.getWindowToken(), 0);
//			}	  
			break;
		default:
			break;
		}
	}
	public void addVolunteer() {
		TimeEntry queryValues =  new  TimeEntry();
		queryValues.setVolunteer_purpose(et_volunteer_purpose.getText().toString());
		queryValues.setActivity_information(et_activity_info.getText().toString());
		queryValues.setSupervisor(valueOfSupervisor);
		queryValues.setDescription(et_descritpion.getText().toString());
		queryValues.setHour(et_hours.getText().toString());
		queryValues.setStartDate(et_start_time.getText().toString());
		queryValues.setEndDate(et_end_time.getText().toString());
		queryValues.setComments(et_supervisor_des.getText().toString());
		queryValues.setvolunteerOrganization(et_volunter_organization.getText().toString());
		queryValues.setNotes(et_note_des.getText().toString());
		controller.insertVolunteer(queryValues);
		controller.close();
		this.callHomeActivity();
	}
	public void callHomeActivity() {
		finish();
		
	}	
	public void resetValue(TimeEntry obj){
		et_volunteer_purpose.setText(obj.getVolunteer_purpose());
		et_activity_info.setText(obj.getActivity_information());
		et_start_time.setText(obj.getStartDate());
		et_end_time.setText(obj.getEndDate());
		et_descritpion.setText(obj.getDescription());
		et_hours.setText(String.valueOf(obj.getHour()));
		et_volunter_organization.setText(String.valueOf(obj.getVolunteerOrganization()));
		et_supervisor_des.setText(String.valueOf(obj.getComment()));
		et_note_des.setText(String.valueOf(obj.notes));
		sp_select_supervisor.setSelection(findSupervisorIndex(obj.getSupervisor()));
	}
	public void editVolunteer() {
		TimeEntry queryValues =  new  TimeEntry();
		queryValues.setVolunteer_purpose(et_volunteer_purpose.getText().toString());
		queryValues.setActivity_information(et_activity_info.getText().toString());
		queryValues.setSupervisor(valueOfSupervisor);
		queryValues.setDescription(et_descritpion.getText().toString());
		queryValues.setHour(et_hours.getText().toString());
		queryValues.setNotes(et_note_des.getText().toString());
		queryValues.setStartDate(et_start_time.getText().toString());
		queryValues.setEndDate(et_end_time.getText().toString());
		queryValues.setComments(et_supervisor_des.getText().toString());
		queryValues.setId(extras.getString("Id"));
		queryValues.setvolunteerOrganization(et_volunter_organization.getText().toString());
		controller.updateVolunteer(queryValues);
		controller.close();
		this.callHomeActivity();
	}
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		if(sp_select_supervisor.getId() == R.id.selectSuperVisorEditText){
			valueOfSupervisor = "";
			if(arg2==0){
			}else{
				valueOfSupervisor = supervisorName[arg2];
			}	
		}
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}	
	private int findSupervisorIndex(String name){
		for(int i = 0 ; i<supervisorName.length;i++){
			if(supervisorName[i].equals(name))
				return i;
		}
		return 0;
	}
	private String findSupervisorId(String name){
		ArrayList<Supervisor> supervisorList =  controller.getAllSupervisor();
		for(int i=0;i<supervisorList.size();i++){
			if(supervisorList.get(i).getName().equals(name))
				return supervisorList.get(i).getId();
		}
		return null;
	}
	private int distanceBetweenDate(long startTime , long endTime){
		long diff = startTime - endTime;
		return  (int) (diff / (1000 * 60 * 60));
	}
	private String makeDateToString(Date date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss",Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(date);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(extras.getInt("value")==1){
			findSupervisorList();
			obj = controller.getVolunteerInfo(extras.getString("Id"));
			resetValue(obj);
		}else{
			findSupervisorList();
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		 switch(v.getId()){
		    case R.id.superVisorDescription:
		    	  if (hasFocus) {
		    		  scoll_view_time_log.setEnableScrolling(false);
	                } else {
	                	scoll_view_time_log.setEnableScrolling(true);
	                }	  
		    	  break;
		    	  
		    case R.id.notesDescription:
		    	if (hasFocus) {
		    		  scoll_view_time_log.setEnableScrolling(false);
	                } else {
	                	scoll_view_time_log.setEnableScrolling(true);
	                }	  
		    	  break;
//		    case R.id.startTimeEditText:
//		    	if (hasFocus)
//		    		et_start_time.setInputType(InputType.TYPE_NULL);
//		    	break;
//		    case R.id.endTimeEditText: 
//		    	if (hasFocus)
//		    		et_end_time.setInputType(InputType.TYPE_NULL);
//		    	break;
		 }
	}
}
