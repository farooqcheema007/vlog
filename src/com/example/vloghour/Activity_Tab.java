package com.example.vloghour;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

public class Activity_Tab extends TabActivity{
	static Context context;
//	TabHost tabHost;
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab);
		context=this;
		
		Resources ressources = getResources(); 
        @SuppressWarnings("deprecation")
		TabHost tabHost = getTabHost();		
        tabHost.getTabWidget().setStripEnabled(false);
        
        
        android.view.Display display = ((android.view.WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int screenWidth = (int)(display.getWidth());
        int screenHeight = (int)(display.getHeight());
        int width = (int)(screenWidth*0.16);
        int height = (int)(screenHeight*0.07);
        int leftMargin = (int)(screenWidth*0.12);
        
        
        tabHost.getTabWidget().setStripEnabled(true);
			
//			TextView tvTabGroup1 = new TextView(this);
//			tvTabGroup1.setText("Time Log");
//			tvTabGroup1.setGravity(Gravity.CENTER_HORIZONTAL| Gravity.CENTER_VERTICAL);
//			tvTabGroup1.
//			tabHost.setPadding(0, 0, 0, bottom);
//        TextView txtTab = new TextView(this);
//        txtTab.setText("Time Log");
////        txtTab.setPadding(8, 9, 8, 9);
//        txtTab.setTextColor(Color.WHITE);
//        txtTab.setTextSize(14);
//        txtTab.setTypeface(localTypeface1);
//        txtTab.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
			tabHost.addTab(tabHost
					.newTabSpec("tab1")
					.setIndicator("",getResources().getDrawable(R.drawable.time))
					.setContent(new Intent(Activity_Tab.this, TabGroup1.class)));

			tabHost.addTab(tabHost
					.newTabSpec("tab2")
					.setIndicator("",getResources().getDrawable(R.drawable.supervisor))
					.setContent(new Intent(Activity_Tab.this, TabGroup2.class)));

			tabHost.addTab(tabHost
					.newTabSpec("tab3")
					.setIndicator("",getResources().getDrawable(R.drawable.fileup))
					.setContent(new Intent(Activity_Tab.this, TabGroup3.class)));
			
			tabHost.addTab(tabHost
					.newTabSpec("tab4")
					.setIndicator("",getResources().getDrawable(R.drawable.profile))
					.setContent(new Intent(Activity_Tab.this, TabGroup4.class)));
			
			tabHost.addTab(tabHost
					.newTabSpec("tab5")
					.setIndicator("Connect")
					.setContent(new Intent(Activity_Tab.this, TabGroup5.class)));
			
			
//			tabHost.addTab(tabHost
//					.newTabSpec("tab3")
//					.setIndicator((""),
//							getResources().getDrawable(R.drawable.ic_launcher))
//					.setContent(new Intent(Activity_Tab.this, Activity_Add_Time_Log.class)));
			
		 
		        //set Windows tab as default (zero based)
//				child 0
			 	LayoutParams paramlv = new LayoutParams(width,height);
		        paramlv.leftMargin = leftMargin;
		        tabHost.setCurrentTab(0);
//		        tabHost.getTabWidget().sett
		        tabHost.getTabWidget()
				.getChildAt(0)
				.setLayoutParams(paramlv);
//		        tabHost.getTabWidget()
//				.getChildAt(0)
//				.setPadding(0, 0, 0, 1);
//		        LinearLayout ll = (LinearLayout) tabHost.getChildAt(0);
//		        TabWidget tw = (TabWidget) ll.getChildAt(0);

		        // for changing the text size of first tab
//		        RelativeLayout rllf = (RelativeLayout) tw.getChildAt(0);
//		        TextView lf = (TextView) tabHost.getTabWidget().getChildAt(0);
//		        lf.setTextSize(21); 
		        
//				child 1
		        tabHost.getTabWidget()
				.getChildAt(1)
				.setLayoutParams(
						new LinearLayout.LayoutParams(width,height));
//		        tabHost.getTabWidget()
//				.getChildAt(1)
//				.setPadding(0, 0, 0, 15);
//				child 2
		        tabHost.getTabWidget()
				.getChildAt(2)
				.setLayoutParams(
						new LinearLayout.LayoutParams(width,height));
//		        tabHost.getTabWidget()
//				.getChildAt(2)
//				.setPadding(0, 0, 0, 15);
//		        tabHost.getTabWidget()
//				.getChildAt(2)
//				.setl
//				child 3
		        tabHost.getTabWidget()
				.getChildAt(3)
				.setLayoutParams(
						new LinearLayout.LayoutParams(width, height));
//		        tabHost.getTabWidget()
//				.getChildAt(3)
//				.setPadding(0, 0, 0, 15);
//				child 4
		        
		        tabHost.getTabWidget()
				.getChildAt(4)
				.setLayoutParams(
						new LinearLayout.LayoutParams(width, height));
//		        tabHost.getTabWidget()
//				.getChildAt(4)
//				.setPadding(0, 0, 0, 15);

		        
//		        tabHost.getTabWidget()
//				.getChildAt(3)
//				.setVisibility(View.INVISIBLE);
		        

//			tabHost.setCurrentTab(1);
//			tabHost.getTabWidget()
//					.getChildAt(0)
//					.setLayoutParams(
//							new LinearLayout.LayoutParams(width / 4, getTabWidget()
//									.getChildAt(0).getLayoutParams().height = 80));
//
//			tabHost.getTabWidget()
//					.getChildAt(1)
//					.setLayoutParams(
//							new LinearLayout.LayoutParams(width / 4, getTabWidget()
//									.getChildAt(1).getLayoutParams().height = 80));
//
//			tabHost.getTabWidget()
//					.getChildAt(2)
//					.setLayoutParams(
//							new LinearLayout.LayoutParams(width / 2, getTabWidget()
//									.getChildAt(2).getLayoutParams().height = 80));
//		}
	}
//	  @Override
//	    public boolean onCreateOptionsMenu(Menu menu) {
//	    	MenuInflater inflater = getMenuInflater();
//	    	inflater.inflate(R.menu.main, menu);
//	    	
//	    	return super.onCreateOptionsMenu(menu);
//	    }
}
