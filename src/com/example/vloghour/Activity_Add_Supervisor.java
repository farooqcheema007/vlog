package com.example.vloghour;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity_Add_Supervisor extends Activity implements OnClickListener{
	EditText et_name, et_title , et_address , et_phone_no , et_organization , et_email;
	Button btn_add_supervisor;
	DBController controller = new DBController(this);
	Bundle extras;
	String supervisorName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_supervisor);
		Registerwidgets();
		extras = getIntent().getExtras();
		if(extras.getInt("value")==1){
			Supervisor obj = controller.getSupervisorInfo(extras.getString("Id"));
			resetValue(obj);
		}else if(extras.getInt("value")==2){
			Supervisor obj = controller.getSupervisorInfo(extras.getString("Id"));
			resetValue(obj);
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		super.onBackPressed();
	}
	private void Registerwidgets(){
		et_name = (EditText)findViewById(R.id.superVisorNameEditText);
		et_title = (EditText)findViewById(R.id.superVisorTitleEditText);
		et_address = (EditText)findViewById(R.id.streetAddressEditText);
		et_phone_no = (EditText)findViewById(R.id.superVisorPhoneNoEditText);
		et_organization = (EditText)findViewById(R.id.organizationNameEditText);
		et_email = (EditText)findViewById(R.id.superVisorEmailEditText);
		btn_add_supervisor = (Button)findViewById(R.id.btnAddSupervisorDataBase);
		
		
		btn_add_supervisor.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAddSupervisorDataBase:
			if(extras.getInt("value")==0)
				addSupervisor();
			else if(extras.getInt("value")==1){
				controller.changeAllSupervisorName(supervisorName, et_name.getText().toString());
				editSupervisor();
			}else if(extras.getInt("value")==2){
				controller.changeAllSupervisorName(supervisorName, et_name.getText().toString());
				editSupervisor();
			}
			break;
			
		default:
			break;
		}

		
		
	}
	public void resetValue(Supervisor obj){
		et_name.setText(obj.getName());
		supervisorName = obj.getName();
		et_title.setText(obj.getTitle());
		et_address.setText(obj.getAddress());
		et_organization.setText(obj.getOrganization());
		et_email.setText(String.valueOf(obj.getEmail()));
		et_phone_no.setText(obj.getPhoneNo());
	}
	public void addSupervisor() {
		Supervisor queryValues =  new  Supervisor();
		queryValues.setName(et_name.getText().toString());
		queryValues.setTitle(et_title.getText().toString());
		queryValues.setAddress(et_address.getText().toString());
		queryValues.setOrganization(et_organization.getText().toString());
		queryValues.setEmail(et_email.getText().toString());
		queryValues.setPhoneNo(et_phone_no.getText().toString());
		controller.insertSupervisor(queryValues);
		controller.close();
		this.callHomeActivity();
	}
	public void editSupervisor() {
		Supervisor queryValues =  new  Supervisor();
		queryValues.setName(et_name.getText().toString());
		queryValues.setTitle(et_title.getText().toString());
		queryValues.setAddress(et_address.getText().toString());
		queryValues.setOrganization(et_organization.getText().toString());
		queryValues.setEmail(et_email.getText().toString());
		queryValues.setPhoneNo(et_phone_no.getText().toString());
		queryValues.setId(extras.getString("Id"));
		controller.updateSupervisor(queryValues);
		controller.close();
		this.callHomeActivity();
	}
	public void callHomeActivity() {
		finish();
		
	}
	
}
