package com.example.vloghour;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Activity_About_Us extends Activity{
	TextView tv_play_google;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);	
		registerWidgets();
		setHyperLink();
	}
	private void registerWidgets(){
		tv_play_google	=	(TextView)findViewById(R.id.PlayStore);
	}
	private void setHyperLink(){
		// setting hyperLink in TextView 
		tv_play_google.setText(Html.fromHtml(
		            " <font color='blue'><u><a href=\"http://www.google.com\">www.google.com</a></u></font>"));
		tv_play_google.setMovementMethod(LinkMovementMethod.getInstance());

	}
}
