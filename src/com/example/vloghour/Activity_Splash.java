package com.example.vloghour;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class Activity_Splash extends Activity {
	 // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    Handler handler = new Handler();
    Runnable runAble;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        runAble = new Runnable() 
        {          
        		@Override
                public void run() {
            // This method will be executed once the timer is over
            // Start your app main activity
            Intent i = new Intent(Activity_Splash.this, Activity_Tab.class);
            startActivity(i);

            // close this activity
            finish();
            }
        };
 
        handler.postDelayed(runAble, SPLASH_TIME_OUT);
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
    	handler.removeCallbacks(runAble);
        super.onBackPressed();

}

}
