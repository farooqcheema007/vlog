package com.example.vloghour;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Activity_Supervisor extends Activity implements OnClickListener{
	Button 		btn_add_supervisor;
	ListView 	lv;
	DBController controller = new DBController(this);
	ArrayList<Supervisor> supervisorList;
	ListAdapter1 adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_supervisor);
		registerwidgets();
		setListener();
		supervisorList =  controller.getAllSupervisor();
//		if(supervisorList.size()!=0) {
			lv.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
				  @Override 
				  public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
			 			Intent intent_add_superviosr = new Intent(Activity_Supervisor.this,Activity_Add_Supervisor.class);
						Bundle bundle	=	new Bundle();
						bundle.putString("Id", supervisorList.get(position).getId()); 
						bundle.putInt("value", 1); 
						intent_add_superviosr.putExtras(bundle);
						startActivity(intent_add_superviosr);
				  }
			});
			adapter = new ListAdapter1();
			lv.setAdapter(adapter);
//		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	private void registerwidgets(){
		lv = (ListView)findViewById(R.id.listViewSupervisor);
		btn_add_supervisor = (Button)findViewById(R.id.btnAddSuperVisor);
	}
	private void setListener(){
		btn_add_supervisor.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAddSuperVisor:
			Intent intent_add_superviosr = new Intent(Activity_Supervisor.this,Activity_Add_Supervisor.class);
			Bundle bundle	=	new Bundle();
			bundle.putInt("value", 0); 
			intent_add_superviosr.putExtras(bundle);
			startActivity(intent_add_superviosr);
			break;

		default:
			break;
		}
	}
	class ListAdapter1 extends ArrayAdapter<Supervisor>{

	 	public ListAdapter1() {
	 		super(Activity_Supervisor.this, R.layout.inflate_listview_textview,supervisorList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_listview_supervisor, null);
 		    }
 			TextView tv_volunteer_purpose = (TextView)convertView.findViewById(R.id.textviewInflateList);
 			Button btn_delete_volunteer = (Button)convertView.findViewById(R.id.btnDeleteTimeLog);
 			btn_delete_volunteer.setOnClickListener(new View.OnClickListener() {
 		         @Override
 		          public void onClick(View v) {
 		        	controller.deleteSupervisor(supervisorList.get(position).getId());
 		        	controller.close();
 		        	adapter.clear();
 		        	supervisorList =  controller.getAllSupervisor();
 		        	adapter.addAll(supervisorList);
 		        	adapter.notifyDataSetChanged();
// 					lv.setAdapter(adapter);

 		           }
 			});
 			tv_volunteer_purpose.setText(supervisorList.get(position).getName());
 			return convertView;	
		}
	 	

	}
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		adapter = new ListAdapter1();
//		lv.setAdapter(adapter);
     	adapter.clear();
     	supervisorList =  controller.getAllSupervisor();
     	adapter.addAll(supervisorList);
     	adapter.notifyDataSetChanged();

	}	
}
