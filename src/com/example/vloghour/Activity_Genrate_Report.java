package com.example.vloghour;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class Activity_Genrate_Report extends Activity implements OnClickListener,OnItemSelectedListener{
	EditText et_start_date , et_end_date;
	Spinner sp_volunter_purpose,sp_volunteer_organization;
	CustomDateTimePickerForReport custom;
	boolean distanceStart = false, distanceFinish= false;
	long secondsStart , secondsEnd;
	Button btnSubmitReport;
	DBController controller = new DBController(this);
	ProgressBar pb;
	static int valueForParagraph=0;
	ArrayAdapter<String> 	adapter_volunteer_purpose;
	String[]				volunteer_purpose;
	ArrayAdapter<String> 	adapter_volunteer_organization;
	String[]				volunteer_organization;
	Date 					start_date , end_date;

//	ProgressDialog 	mProgressDialog;
//	ArrayList<TimeEntry> arrTime;
	
	
	File file = new File(Environment.getExternalStorageDirectory()+File.separator+"Report.pdf");
//    private static Font paragraphFont = new Font(Font.FontFamily.TIMES_ROMAN, 50,
//            Font.NORMAL);
//    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
//            Font.NORMAL, BaseColor.RED);
//    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
//            Font.BOLD);
//    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
//            Font.BOLD);
    static Context  context;
    boolean flagNextPage = false;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		View viewToLoad = LayoutInflater.from(this.getParent()).inflate(R.layout.activity_generte_report, null);
		this.setContentView(viewToLoad); 
//		setContentView(R.layout.activity_generte_report);
		registerWidgets();
		editTextOnFocusChange();
		getAllVolunterDataFromDataBase();
		setListner();
		context = this;
	}
	private void registerWidgets(){
		et_start_date = (EditText)findViewById(R.id.startTimeEditText);
		et_end_date = (EditText)findViewById(R.id.endTimeEditText);
		btnSubmitReport = (Button)findViewById(R.id.btnGenerteReport);
		pb = (ProgressBar)findViewById(R.id.progressBarGenerateReport); 
//		et_volunteer_purpose=(EditText)findViewById(R.id.volunteerPurposeEdit);
		sp_volunter_purpose = (Spinner)findViewById(R.id.volunteerPurposeSpinner);
		sp_volunteer_organization =(Spinner)findViewById(R.id.volunteerOrganizationSpinner);		
	}
	  private void setListner(){
		sp_volunter_purpose.setOnItemSelectedListener(this);
		pb.setVisibility(View.INVISIBLE);
		btnSubmitReport.setOnClickListener(this);
		et_start_date.setOnClickListener(this);
		et_end_date.setOnClickListener(this);

	}   
	private void getAllVolunterDataFromDataBase(){
		ArrayList<TimeEntry> volunteerList =  controller.getAllVolunteerGroupBy();
		volunteer_purpose=new String[volunteerList.size()+1];
		volunteer_purpose[0] = "Select Volunteer Purpose";
		for(int i = 0;i <volunteerList.size();i++){
			volunteer_purpose[i+1] = volunteerList.get(i).getVolunteer_purpose();
		}
		
		adapter_volunteer_purpose = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,volunteer_purpose);
		adapter_volunteer_purpose.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_volunter_purpose.setSelection(volunteer_purpose.length);
		sp_volunter_purpose.setAdapter(adapter_volunteer_purpose);
		
		volunterOrganization();
		
	}
	private void volunterOrganization(){
		volunteer_organization=new String[1];
		volunteer_organization[0] = "Select Volunteer Organization";
		adapter_volunteer_organization = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,volunteer_organization);
		adapter_volunteer_organization.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_volunteer_organization.setSelection(volunteer_organization.length);
		sp_volunteer_organization.setAdapter(adapter_volunteer_organization);
		sp_volunteer_organization.setEnabled(false);
		sp_volunteer_organization.setClickable(false);
	}
//	private Context getDialogContext() {
//	    Context context;
//	    if (getParent() != null) context = getParent();
//	    else context = this;
//	    return context;
//	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		getAllVolunterDataFromDataBase();
		super.onResume();
	}
	private void dateAndTimePickerStartTime(){
		 custom = new CustomDateTimePickerForReport(this,
		            new CustomDateTimePickerForReport.ICustomDateTimeListener() {
		                @Override
		                public void onCancel() {

		                }

						@Override
						public void onSet(Dialog dialog,
								Calendar calendarSelected, Date dateSelected,
								int year, String monthFullName,
								String monthShortName, int monthNumber,
								int date, String weekDayFullName,
								String weekDayShortName, int hour24) {
							// TODO Auto-generated method stub
							
							et_start_date.setText(makeDateToString(dateSelected,true));
//							dateStart = dateSelected;
//							et_start_time .setText(calendarSelected
//                               .get(Calendar.DAY_OF_MONTH)
//                               + "/" + (monthNumber+1) + "/" + year
//                               + ", " + hour12 + ":" + min
//                               + " " + AM_PM);
							secondsStart = dateSelected.getTime();
//							if(distanceHoursStart)
//								et_hours.setText(String.valueOf(distanceBetweenDate(secondsEnd,secondsStart)));

						}
		            });
		 
		 custom.set24HourFormat(false);
		 /**
		  * Pass Directly current data and time to show when it pop up
		  */
		 custom.setDate(Calendar.getInstance());
	}
	private void dateAndTimePickerEndTime(){
		 custom = new CustomDateTimePickerForReport(this,
		            new CustomDateTimePickerForReport.ICustomDateTimeListener() {
		                @Override
		                public void onCancel() {

		                }

						@Override
						public void onSet(Dialog dialog,
								Calendar calendarSelected, Date dateSelected,
								int year, String monthFullName,
								String monthShortName, int monthNumber,
								int date, String weekDayFullName,
								String weekDayShortName, int hour24) {
							// TODO Auto-generated method stub
							
//							DateFinal = dateSelected;
							et_end_date.setText(makeDateToString(dateSelected,false));
//							et_end_time.setText(calendarSelected
//                              .get(Calendar.DAY_OF_MONTH)
//                              + "/" + (monthNumber+1) + "/" + year
//                              + ", " + hour12 + ":" + min
//                              + " " + AM_PM);
							secondsEnd = dateSelected.getTime();
//							if(distanceHoursEnd)
//								et_hours.setText(String.valueOf(distanceBetweenDate(secondsEnd,secondsStart)));
							
								
						}
		            });
		 
		 custom.set24HourFormat(false);
		 /**
		  * Pass Directly current data and time to show when it pop up
		  */
		 custom.setDate(Calendar.getInstance());
	}
	
	private void editTextOnFocusChange(){
		et_start_date.setInputType(InputType.TYPE_NULL);
		et_end_date.setInputType(InputType.TYPE_NULL);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.startTimeEditText:
			pb.setVisibility(View.VISIBLE);
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(et_start_date.getWindowToken(), 0);			
			dateAndTimePickerStartTime();
			distanceFinish = true;
			if(distanceStart){
				custom.settingMaxDate(secondsEnd);
				distanceFinish = false;
			}
			pb.setVisibility(View.INVISIBLE);
			custom.showDialog();			 
			break;
		case R.id.endTimeEditText:
			InputMethodManager immm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			immm.hideSoftInputFromWindow(et_end_date.getWindowToken(), 0);			
			dateAndTimePickerEndTime();
			distanceStart = true;
			if(distanceFinish){
				custom.settingMinDate(secondsStart);
				distanceStart = false;
			}
			custom.showDialog();
			break;
		case R.id.btnGenerteReport:
			if (et_start_date.getText().toString().matches("")) 
			    Toast.makeText(this, "Please Enter Start Date..", Toast.LENGTH_SHORT).show();
			else if(et_end_date.getText().toString().matches(""))
				Toast.makeText(this, "Please Enter End Date..", Toast.LENGTH_SHORT).show();
//			else if(et_volunteer_purpose.getText().toString().matches(""))
//				Toast.makeText(this, "Please Enter Volunteer Purpose...", Toast.LENGTH_SHORT).show();
//			else if(et_volunteer_organization.getText().toString().matches(""))
//				Toast.makeText(this, "Please Enter Volunteer Organization...", Toast.LENGTH_SHORT).show();
			else 
				new SendEmail().execute();
			break;
		default:	
			break;
		}
	}
	private String makeDateToString(Date date,boolean flag){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		if(flag)
			start_date = date;
		else
			end_date = date;
		return dateFormat.format(date);
	}
	private String makeDateToStringForResult(Date date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(date);
	}

	private void makePdfReport(){
		ArrayList<TimeEntry> arrTime = controller.findAllDataForReport(makeDateToStringForResult(start_date)+" 00:00:01", makeDateToStringForResult(end_date)+" 23:59:59",
				sp_volunter_purpose.getSelectedItem().toString(),sp_volunteer_organization.getSelectedItem().toString());
		ArrayList<Supervisor> arrSupervisor = controller.getAllSupervisor();
		
		try {
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(file));
				document.open();
				addMetaData(document);
				addTitlePage(document,arrTime,arrSupervisor);
//				addContent(document);	
				document.close();

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		
	}
	
//	private void getDataReportDatabase(){
//		
//	}
	private void addMetaData(Document document) {
	        document.addTitle("Report");
//	        document.addSubject("Using iText");
//	        document.addKeywords("Java, PDF, iText");
//	        document.addAuthor("Lars Vogel");
//	        document.addCreator("Lars Vogel");
	    }

	    private void addTitlePage(Document document,ArrayList<TimeEntry> arrTime,ArrayList<Supervisor> arrSupervisor)
	            throws DocumentException {
	    	valueForParagraph=0;
	    	addFooterFirst(document);
	    	addHeader(document);
	    	for (int i=0;i<arrTime.size();i++){
	    		// Volunteer Purpose
	    		Paragraph paraVolunterPurpose = new Paragraph("Volunteering Purpose: "+arrTime.get(i).getVolunteer_purpose());
	    		mainCalculationForHeaderAndFooter(paraVolunterPurpose.toString(),document);
	    		paraVolunterPurpose.setSpacingAfter(10);
	    		paraVolunterPurpose.getContent().length();
	    		paraVolunterPurpose.getFont().setSize(18);
	    		document.add(paraVolunterPurpose);
	    		// Volunteer Purpose
	    		Paragraph paraVolunterOrganization = new Paragraph("Volunter Organization: "+arrTime.get(i).getVolunteerOrganization());
	    		mainCalculationForHeaderAndFooter(paraVolunterOrganization.toString(),document);
	    		paraVolunterOrganization.setSpacingAfter(10);
	    		paraVolunterOrganization.getFont().setSize(18);
	    		document.add(paraVolunterOrganization);
	    		paraVolunterOrganization.getContent().length();
	    		// Info Activity
	    		Paragraph paraInfoActivity = new Paragraph("Activity Information:"+arrTime.get(i).getActivity_information());
	    		mainCalculationForHeaderAndFooter(paraInfoActivity.toString(),document);
	    		paraInfoActivity.setSpacingAfter(10);
	    		paraInfoActivity.getFont().setSize(18);
	    		document.add(paraInfoActivity);
	    		paraInfoActivity.getContent().length();
	    		// Date
	    		Paragraph paraDate = new Paragraph("Date: "+arrTime.get(i).getStartDate());
	    		mainCalculationForHeaderAndFooter(paraDate.toString(),document);
	    		paraDate.setSpacingAfter(10);
	    		paraDate.getFont().setSize(18);
	    		document.add(paraDate);
	    		// Hours 
	    		Paragraph paraHours = new Paragraph("Hours:"+arrTime.get(i).getHour());
	    		mainCalculationForHeaderAndFooter(paraHours.toString(),document);
	    		paraHours.setSpacingAfter(10);
	    		paraHours.getFont().setSize(18);
	    		document.add(paraHours);
	    		//Supervisor Comment
	    		Paragraph paraSupervisorComment = new Paragraph("Supervisor's Comments: "+arrTime.get(i).getComment());
	    		mainCalculationForHeaderAndFooter(paraSupervisorComment.toString(),document);
	    		paraSupervisorComment.setSpacingAfter(20);
	    		paraSupervisorComment.getFont().setSize(18);
	    		paraSupervisorComment.getContent().length();		
	    		document.add(paraSupervisorComment);
	    		//Supervisor 
	    		Paragraph paraSupervisor = new Paragraph("Organization Where Volunteered");
	    		mainCalculationForHeaderAndFooter(paraSupervisor.toString(),document);
	    		paraSupervisor.setSpacingAfter(10);
	    		paraSupervisor.getFont().setSize(18);
	    		document.add(paraSupervisor);
	    		// For Supervisor
	    		for(int j=0;j<arrSupervisor.size();j++){
	    			if(arrTime.get(i).getSupervisor().equals(arrSupervisor.get(j).getName())){
	    				//Supervisor Organization Name
	    	    		Paragraph paraSupervisorOrg = new Paragraph("Organizationís Name: "+arrSupervisor.get(j).getOrganization());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorOrg.toString(),document);
	    	    		paraSupervisorOrg.setSpacingAfter(10);
	    	    		paraSupervisorOrg.getFont().setSize(18);
	    	    		document.add(paraSupervisorOrg);
	    				//Supervisor Street Address 
	    	    		Paragraph paraSupervisorAddress = new Paragraph("Street Address: "+arrSupervisor.get(j).getAddress());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorAddress.toString(),document);
	    	    		paraSupervisorAddress.setSpacingAfter(10);
	    	    		paraSupervisorAddress.getFont().setSize(18);
	    	    		document.add(paraSupervisorAddress);
	    	    		//Supervisor Name
	    	    		Paragraph paraSupervisorName = new Paragraph("Supervisor's Name: "+arrTime.get(i).getSupervisor());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorName.toString(),document);
	    	    		paraSupervisorName.setSpacingAfter(10);
	    	    		paraSupervisorName.getFont().setSize(18);
	    	    		document.add(paraSupervisorName);
	    	    		//Supervisor Title
	    	    		Paragraph paraSupervisorTitle = new Paragraph("Supervisor's Title: "+arrSupervisor.get(j).getTitle());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorTitle.toString(),document);
	    	    		paraSupervisorTitle.setSpacingAfter(10);
	    	    		paraSupervisorTitle.getFont().setSize(18);
	    	    		document.add(paraSupervisorTitle);
	    	    		//Supervisor Phone
	    	    		Paragraph paraSupervisorPhone = new Paragraph("Supervisor's Phone: "+arrSupervisor.get(j).getPhoneNo());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorPhone.toString(),document);
	    	    		paraSupervisorPhone.setSpacingAfter(10);
	    	    		paraSupervisorPhone.getFont().setSize(18);
	    	    		document.add(paraSupervisorPhone);
	    	    		//Supervisor Email
	    	    		Paragraph paraSupervisorEmail = new Paragraph("Supervisor's Email: "+arrSupervisor.get(j).getEmail());
	    	    		mainCalculationForHeaderAndFooter(paraSupervisorEmail.toString(),document);
	    	    		paraSupervisorEmail.setSpacingAfter(10);
	    	    		paraSupervisorEmail.getFont().setSize(18);
	    	    		document.add(paraSupervisorEmail);
	    			}
	    		}
	    		
	    	}
	    	
	    }
	    private static void addHeader(Document document){
	    	try {
	    		InputStream ims = context.getAssets().open("img_pdf_header.png");
	    		Bitmap bmp = BitmapFactory.decodeStream(ims);
	    		ByteArrayOutputStream stream = new ByteArrayOutputStream(); 	
	    		bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
	    		Image image = Image.getInstance(stream.toByteArray());
	    		image.scalePercent(16f);
	    		image.getScaledHeight();
				document.add(image);
			}
	        catch(Exception ex) {
	            return;
	        }
	    }
	    public void mainCalculationForHeaderAndFooter(String strParagh,Document doc){
//	    	strParagh.
	    	int valueNewLine = valueNewLine(strParagh);
	    	if(valueNewLine>2)
	    		valueNewLine = newLineMode(valueNewLine);
	    	int value = paraGraphLine(strParagh.length());
//	    	valueForParagraph = value + valueForParagraph;
	    	if((valueForParagraph+value)>17&&!flagNextPage){
	    		valueForParagraph = 0;
	    		flagNextPage = true;
	    		doc.newPage();
		    	addFooter(doc);
		    	addHeader(doc);
	    	}else if((valueForParagraph+value)>16&&flagNextPage){
	    		valueForParagraph = 0;
	    		doc.newPage();
		    	addFooter(doc);
		    	addHeader(doc);
	    	}else{
	    		valueForParagraph = value + valueForParagraph+(valueNewLine-1);
	    	}
	    }
	    public int newLineMode(int value){
	    	int divisor = value/2;
	    	int mode = value%2;
	    	return divisor+mode;
	    }
	    public int valueNewLine(String str){
	    	return str.split("\n").length;
	    }
	    public int paraGraphLine(int length){
	    	if(length<61)
	    		return 1;
	    	else{
	    		int value=1;
	    		for(int i=0;i<length;i++){
	    			if(length>60){
	    				length=length/2;
	    				value++;
	    			}else
	    				return value;
	    		}
	    	}
	    	return 1;
	    }
	    private static void addFooter(Document document){
	    	try {
		           InputStream ins = context.getAssets().open("img_pdf_footer.png");
		           Bitmap b = BitmapFactory.decodeStream(ins);
		           ByteArrayOutputStream streamByte = new ByteArrayOutputStream(); 	
		           b.compress(Bitmap.CompressFormat.PNG, 100, streamByte);
		           Image imageBottom = Image.getInstance(streamByte.toByteArray());
		           imageBottom.setAbsolutePosition(0, 0);
		           imageBottom.scalePercent(17f);
		           Chunk chunk = new Chunk(imageBottom, 0, -770f);
		           document.add(chunk);
			}
	        catch(Exception ex) {
	            return;
	        }
	    }
//
	    private static void addFooterFirst(Document document){
	    	try {
		           InputStream ins = context.getAssets().open("img_pdf_footer.png");
		           Bitmap b = BitmapFactory.decodeStream(ins);
		           ByteArrayOutputStream streamByte = new ByteArrayOutputStream(); 	
		           b.compress(Bitmap.CompressFormat.PNG, 100, streamByte);
		           Image imageBottom = Image.getInstance(streamByte.toByteArray());
		           imageBottom.scalePercent(17f);
		           Chunk chunk = new Chunk(imageBottom, 0, -800f);
		           document.add(chunk);
			}
	        catch(Exception ex) {
	            return;
	        }
	    }
//	    private static int setPDFPageY(int value){
//	    	if(value==150)
//	    		return 0;
//	    	else
//	    		return value;
//	    }
//	    private static int countLines(String str){
//	    	   String[] lines = str.split("\r\n|\r|\n");
//	    	   return  lines.length;
//	    	}
	    private void sendMail(){
			  Intent email = new Intent(Intent.ACTION_SEND,Uri.parse("mailto:"));
			  
//			  Intent intent = new Intent(Intent.ACTION_SEND ,); // it's not ACTION_SEND
			  email.setType("text/plain");
//			  email.putExtra(Intent.EXTRA_SUBJECT, "Card Set ");
//			  email.putExtra(Intent.EXTRA_TEXT, "");
			  email.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(file));
			  email.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			  email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "cheemafarooq28@gmail.com"});
			  
//			  email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
//			  //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
//			  //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
			  email.putExtra(Intent.EXTRA_SUBJECT, "testing");
//			  email.putExtra(Intent.EXTRA_TEXT, message);

			  //need this to prompts email client only
			  email.setType("message/rfc822");
			  
			  startActivity(Intent.createChooser(email, "Choose an Email client :"));

	    }
	    
	    private class SendEmail extends AsyncTask<Void, Void, Void> {
	    	 
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            // Create a progressdialog
	            pb.setVisibility(View.VISIBLE);
//	            mProgressDialog = new ProgressDialog(Activity_Genrate_Report.this);
////	            // Set progressdialog title
//	            mProgressDialog.setTitle("Making PDF....");
////	            // Set progressdialog message
////	            mProgressDialog.setMessage("Making PDF....");
//	            mProgressDialog.setIndeterminate(false);
//	             //Show progressdialog
//	            mProgressDialog.show();
	        }
	 
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				makePdfReport();
				return null;
			}

	        @Override
	        protected void onPostExecute(Void v) {
	            // Set the bitmap into ImageView
//	        	iv_profile_image.setImageBitmap(result);
	        	pb.setVisibility(View.INVISIBLE);
	        	sendMail();
	            // Close progressdialog
//	            mProgressDialog.dismiss();
	        }

	    }

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			Spinner spinner = (Spinner) arg0;
			 if(spinner.getId() == R.id.volunteerPurposeSpinner){
				 if(!spinner.getSelectedItem().toString().equals("Select Volunteer Purpose")){
					 sp_volunteer_organization.setEnabled(true);
					 sp_volunteer_organization.setClickable(true);
					 ArrayList<TimeEntry> volunteerList =  controller.getAllVolunteerOrganization(spinner.getSelectedItem().toString());
					 volunteer_organization=new String[volunteerList.size()];
						for(int i = 0;i <volunteerList.size();i++){
							volunteer_organization[i] = volunteerList.get(i).getVolunteerOrganization();
						}
						
						adapter_volunteer_organization = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,volunteer_organization);
						adapter_volunteer_organization.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						sp_volunteer_organization.setSelection(volunteer_organization.length);
						sp_volunteer_organization.setAdapter(adapter_volunteer_organization);
				 }else{
					 volunterOrganization();
				 }
			 }
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	    
	    
}
