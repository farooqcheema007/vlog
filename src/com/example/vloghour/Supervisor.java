package com.example.vloghour;

public class Supervisor {
	String 	supervisor_id;
	String 	supervisor_name;
	String 	supervisor_title;
	String 	supervisor_phone_no;
	String 	supervisor_email; 
	String 	supervisor_organization;
	String 	supervisor_address;
	
	// constructors
	public Supervisor(){
		supervisor_id = null;
		supervisor_name = null;
		supervisor_title = null;
		supervisor_address = null;
		supervisor_email = null;
		supervisor_organization = null;
		supervisor_phone_no = null;
	}
	
	// setter
	public void setId(String supervisor_id) {
		this.supervisor_id = supervisor_id;
	}
	public void setName(String supervisor_name) {
		this.supervisor_name = supervisor_name;
	}
	public void setTitle(String supervisor_title) {
		this.supervisor_title = supervisor_title;
	}
	public void setEmail(String supervisor_email) {
		this.supervisor_email = supervisor_email;
	}
	public void setPhoneNo(String supervisor_phone_no){
		this.supervisor_phone_no = supervisor_phone_no;
	}
	public void setOrganization(String supervisor_organization) {
		this.supervisor_organization = supervisor_organization;
	}
	public void setAddress(String supervisor_address) {
		this.supervisor_address = supervisor_address;
	}
	
	// getter
	public String getId() {
		return this.supervisor_id;
	}
	public String getAddress() {
		return this.supervisor_address;
	}
	public String getOrganization() {
		return this.supervisor_organization;
	}
	public String getPhoneNo(){
		return this.supervisor_phone_no;
	}
	public String getEmail() {
		return this.supervisor_email;
	}
	public String getName() {
		return this.supervisor_name;
	}
	public String getTitle() {
		return this.supervisor_title;
	}

}
