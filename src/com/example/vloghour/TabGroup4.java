package com.example.vloghour;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

public class TabGroup4 extends TabGroupActivity{
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        startChildActivity("", new Intent(this,Activity_Profile.class));
	    }
	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			if ((keyCode == KeyEvent.KEYCODE_BACK)) {

				 super.onBackPressed();
				    finish();
			}
			return super.onKeyDown(keyCode, event);
		}
}
