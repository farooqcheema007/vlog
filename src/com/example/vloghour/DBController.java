package com.example.vloghour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBController extends SQLiteOpenHelper{
	
	private static final String LOGCAT = null;
	public DBController(Context context) {
		super(context, "androidsqlite.db", null, 1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		String query;
		query = "CREATE TABLE volunteer ( volunteerId INTEGER PRIMARY KEY AUTOINCREMENT,volunteer_purpose TEXT ,activity_information TEXT , " +
				" description TEXT , supervisor TEXT , hours INTEGER , start_date date , end_date date, comment TEXT , volunteer_organization TEXT , notes TEXT)";
		arg0.execSQL(query);
        Log.d(LOGCAT,"Volunteer Table Created");
        
        // superviosr table
        
		String querySuperviosr;
		querySuperviosr = "CREATE TABLE superviosr ( superviosrId INTEGER PRIMARY KEY AUTOINCREMENT,superviosr_name TEXT ,superviosr_title TEXT , superviosr_address TEXT ," +
				" superviosr_email TEXT ,superviosr_organization TEXT ,superviosr_phone TEXT)";
		arg0.execSQL(querySuperviosr);
        Log.d(LOGCAT,"Superviosr Table Created");

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	// Volunteer tables Quries
	
	
	public ArrayList<TimeEntry> getAllVolunteerGroupBy() {
		ArrayList<TimeEntry> wordList = new ArrayList<TimeEntry>();
		String selectQuery = "SELECT  * FROM volunteer GROUP BY volunteer_purpose";
	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	TimeEntry map = new TimeEntry();
	        	map.setId(cursor.getString(0));
	        	map.setVolunteer_purpose(cursor.getString(1));
	        	map.setActivity_information(cursor.getString(2));
	        	map.setDescription(cursor.getString(3));
	        	map.setSupervisor(cursor.getString(4));
	        	map.setHour(cursor.getString(5));
	        	map.setStartDate(cursor.getString(6));
	        	map.setEndDate(cursor.getString(7));
	        	map.setComments(cursor.getString(8));
	        	map.setvolunteerOrganization(cursor.getString(9));
	        	map.setNotes(cursor.getString(10));
                wordList.add(map);
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    // return contact list
	    return wordList;
	}
	public ArrayList<TimeEntry> getAllVolunteerOrganization(String VolunteerOrganization) {
		ArrayList<TimeEntry> wordList = new ArrayList<TimeEntry>();
		String selectQuery = "SELECT * FROM volunteer where volunteer_purpose ='"+VolunteerOrganization+"'";
	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	TimeEntry map = new TimeEntry();
	        	map.setId(cursor.getString(0));
	        	map.setVolunteer_purpose(cursor.getString(1));
	        	map.setActivity_information(cursor.getString(2));
	        	map.setDescription(cursor.getString(3));
	        	map.setSupervisor(cursor.getString(4));
	        	map.setHour(cursor.getString(5));
	        	map.setStartDate(cursor.getString(6));
	        	map.setEndDate(cursor.getString(7));
	        	map.setComments(cursor.getString(8));
	        	map.setvolunteerOrganization(cursor.getString(9));
	        	map.setNotes(cursor.getString(10));
                wordList.add(map);
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    // return contact list
	    return wordList;
	}

	public void insertVolunteer(TimeEntry queryValues) {
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("volunteer_purpose", queryValues.getVolunteer_purpose());
		values.put("activity_information", queryValues.getActivity_information());
		values.put("start_date", queryValues.getStartDate());
		values.put("end_date", queryValues.getEndDate());
		values.put("description", queryValues.getDescription());
		values.put("supervisor", queryValues.getSupervisor());
		values.put("hours", queryValues.getHour());
		values.put("comment", queryValues.getComment());
		values.put("volunteer_organization", queryValues.getVolunteerOrganization());
		values.put("notes", queryValues.getnotes());
		database.insert("volunteer", null, values);
		database.close();
	}
	public void updateVolunteer(TimeEntry queryValues) {
		SQLiteDatabase database = this.getWritableDatabase();	 
	    ContentValues values = new ContentValues();
		values.put("volunteer_purpose", queryValues.getVolunteer_purpose());
		values.put("activity_information", queryValues.getActivity_information());
		values.put("start_date", queryValues.getStartDate());
		values.put("end_date", queryValues.getEndDate());
		values.put("description", queryValues.getDescription());
		values.put("supervisor", queryValues.getSupervisor());
		values.put("hours", queryValues.getHour());
		values.put("comment", queryValues.getComment());
		values.put("volunteer_organization", queryValues.getVolunteerOrganization());
		values.put("notes", queryValues.getnotes());
	    database.update("volunteer", values, "volunteerId" + " = ?", new String[] { queryValues.getId() });
	    database.close();
	}
	public void deleteVolunteer(String id) {
		Log.d(LOGCAT,"delete");
		SQLiteDatabase database = this.getWritableDatabase();	 
		String deleteQuery = "DELETE FROM  volunteer where volunteerId='"+ id +"'";
		Log.d("query",deleteQuery);		
		database.execSQL(deleteQuery);
		database.close();
		
	}
	public ArrayList<TimeEntry> getAllVolunteer() {
		ArrayList<TimeEntry> wordList = new ArrayList<TimeEntry>();
		String selectQuery = "SELECT  * FROM volunteer";
	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	TimeEntry map = new TimeEntry();
	        	map.setId(cursor.getString(0));
	        	map.setVolunteer_purpose(cursor.getString(1));
	        	map.setActivity_information(cursor.getString(2));
	        	map.setDescription(cursor.getString(3));
	        	map.setSupervisor(cursor.getString(4));
	        	map.setHour(cursor.getString(5));
	        	map.setStartDate(cursor.getString(6));
	        	map.setEndDate(cursor.getString(7));
	        	map.setComments(cursor.getString(8));
	        	map.setvolunteerOrganization(cursor.getString(9));
	        	map.setNotes(cursor.getString(10));
                wordList.add(map);
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    // return contact list
	    return wordList;
	}
	
	public TimeEntry getVolunteerInfo(String id) {
		TimeEntry wordList = new TimeEntry();
		SQLiteDatabase database = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM volunteer where volunteerId='"+id+"'";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	        	wordList.setId(cursor.getString(0));
	        	wordList.setVolunteer_purpose(cursor.getString(1));
	        	wordList.setActivity_information(cursor.getString(2));
	        	wordList.setDescription(cursor.getString(3));
	        	wordList.setSupervisor(cursor.getString(4));
	        	wordList.setHour(cursor.getString(5));
	        	wordList.setStartDate(cursor.getString(6));
	        	wordList.setEndDate(cursor.getString(7));
	        	wordList.setComments(cursor.getString(8));
	        	wordList.setvolunteerOrganization(cursor.getString(9));
	        	wordList.setNotes(cursor.getString(10));
	        } while (cursor.moveToNext());
	    }
		database.close();
	return wordList;
	}
	
	public void changeSupervisorName(String name,String Id){
		SQLiteDatabase database = this.getWritableDatabase();	 
	    ContentValues values = new ContentValues();
		values.put("supervisor", name);
		database.update("volunteer", values, "volunteerId" + " = ?", new String[] { Id });
	    database.close();
		
	}
	public void changeAllSupervisorName(String oldName ,String newName){
		ArrayList<String> arrayList = new ArrayList<String>(); 
		String selectQuery = "SELECT  * FROM volunteer";
	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	if(oldName.equals(cursor.getString(4)))
	        		arrayList.add(cursor.getString(0));
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    
		SQLiteDatabase databaseVolunteer = this.getWritableDatabase();	 
	    ContentValues values = new ContentValues();
	    values.put("supervisor", newName);
	    for(int i = 0 ;i<arrayList.size();i++){
	    	databaseVolunteer.update("volunteer", values, "volunteerId" + " = ?", new String[] { arrayList.get(i) });
	    }
		databaseVolunteer.close();
		
	}

	public ArrayList<TimeEntry> findAllDataForReport(String startTime , String endTime,String volunteer_purpose,String volunteer_organization){
		ArrayList<TimeEntry> wordList = new ArrayList<TimeEntry>();
		String selectQuery = "SELECT  * FROM volunteer where start_date >= '"+startTime+"' AND end_date <= '"+endTime+"'" +
					" and volunteer_purpose = '"+volunteer_purpose+"'" + "and volunteer_organization = '"+ volunteer_organization+"'";

//		String selectQuery = "SELECT  * FROM volunteer where BETWEEN strftime('%s', "+startTime+") AND strftime('%s', "+endTime+")"+ 
//				" and volunteer_purpose = '"+volunteer_purpose+"'" + "and volunteer_organization = '"+ volunteer_organization+"'";

	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	TimeEntry map = new TimeEntry();
	        	map.setId(cursor.getString(0));
	        	map.setVolunteer_purpose(cursor.getString(1));
	        	map.setActivity_information(cursor.getString(2));
	        	map.setDescription(cursor.getString(3));
	        	map.setSupervisor(cursor.getString(4));
	        	map.setHour(cursor.getString(5));
	        	map.setStartDate(cursor.getString(6));
	        	map.setEndDate(cursor.getString(7));
	        	map.setComments(cursor.getString(8));
	        	map.setvolunteerOrganization(cursor.getString(9));
	        	map.setNotes(cursor.getString(10));
                wordList.add(map);
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    return wordList;		
	}
	
	
	
	// Supervisor Table Quries
	
	public void insertSupervisor(Supervisor queryValues) {
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("superviosr_name", queryValues.getName());
		values.put("superviosr_title", queryValues.getTitle());
		values.put("superviosr_address", queryValues.getAddress());
		values.put("superviosr_phone", queryValues.getPhoneNo());
		values.put("superviosr_email", queryValues.getEmail());
		values.put("superviosr_organization", queryValues.getOrganization());
		database.insert("superviosr", null, values);
		database.close();
	}
	public void updateSupervisor(Supervisor queryValues) {
		SQLiteDatabase database = this.getWritableDatabase();	 
	    ContentValues values = new ContentValues();
		values.put("superviosr_name", queryValues.getName());
		values.put("superviosr_title", queryValues.getTitle());
		values.put("superviosr_address", queryValues.getAddress());
		values.put("superviosr_phone", queryValues.getPhoneNo());
		values.put("superviosr_email", queryValues.getEmail());
		values.put("superviosr_organization", queryValues.getOrganization());
	    database.update("superviosr", values, "superviosrId" + " = ?", new String[] { queryValues.getId() });
	    database.close();
	}
	public void deleteSupervisor(String id) {
		Log.d(LOGCAT,"delete");
		SQLiteDatabase database = this.getWritableDatabase();	 
		String deleteQuery = "DELETE FROM  superviosr where superviosrId='"+ id +"'";
		Log.d("query",deleteQuery);		
		database.execSQL(deleteQuery);
		database.close();
		
	}
	public ArrayList<Supervisor> getAllSupervisor() {
		ArrayList<Supervisor> wordList;
		wordList = new ArrayList<Supervisor>();
		String selectQuery = "SELECT  * FROM superviosr";
	    SQLiteDatabase database = this.getWritableDatabase();
	    Cursor cursor = database.rawQuery(selectQuery, null);
	    if (cursor.moveToFirst()) {
	        do {
	        	Supervisor map = new Supervisor();
	        	map.setId(cursor.getString(0));
	        	map.setName(cursor.getString(1));
	        	map.setTitle(cursor.getString(2));
	        	map.setAddress(cursor.getString(3));
	        	map.setEmail(cursor.getString(4));
	        	map.setOrganization(cursor.getString(5));
	        	map.setPhoneNo(cursor.getString(6));
                wordList.add(map);
	        } while (cursor.moveToNext());
	    }
	    database.close();
	    // return contact list
	    return wordList;
	}
	
	public Supervisor getSupervisorInfo(String id) {
		Supervisor map = new Supervisor();
		SQLiteDatabase database = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM superviosr where superviosrId='"+id+"'";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	        	map.setId(cursor.getString(0));
	        	map.setName(cursor.getString(1));
	        	map.setTitle(cursor.getString(2));
	        	map.setAddress(cursor.getString(3));
	        	map.setEmail(cursor.getString(4));
	        	map.setOrganization(cursor.getString(5));
	        	map.setPhoneNo(cursor.getString(6));
	        } while (cursor.moveToNext());
	    }	
		database.close();
		return map;
	}

	
	
	

}
