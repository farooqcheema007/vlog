package com.example.vloghour;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Activity_Time_Log extends Activity implements OnClickListener{
	ListView 	lv;
	Button 		btn_add_time_log;
	DBController controller = new DBController(this);
	ArrayList<TimeEntry> volunteerList;
	ListAdapter1 adapter;
	static Activity_Time_Log activity_time;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_log);
		registerwidgets();
		setListener();
		createMenu();
//		Activity_Time_Log.this.openOptionsMenu();
//		Menu menu=newMenuInstance(Activity_Time_Log.this);
//		onCreateOptionsMenu(menu);
		activity_time = this;
		volunteerList =  controller.getAllVolunteer();
//		if(volunteerList.size()!=0) {
			lv.setOnItemClickListener(new OnItemClickListener() {
				  @Override 
				  public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
//					  animalId = (TextView) view.findViewById(R.id.animalId);
//					  String valAnimalId = animalId.getText().toString();				
			 			Intent intent_add_time = new Intent(Activity_Time_Log.this,Activity_Add_Time_Log.class);
						Bundle bundle	=	new Bundle();
						bundle.putString("Id", volunteerList.get(position).getId()); 
						bundle.putInt("value", 1); 
						intent_add_time.putExtras(bundle);
						startActivity(intent_add_time);
				  }
			});
			adapter = new ListAdapter1();
			lv.setAdapter(adapter);
//		}

	}
	private void createMenu(){
		try {
	        ViewConfiguration config = ViewConfiguration.get(this);
	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if(menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception ex) {
	        // Ignore
	    }
	}
//	protected Menu newMenuInstance(Context context) {
//	    try {
//	        Class<?> menuBuilderClass = Class.forName("com.android.internal.view.menu.MenuBuilder");
//
//	        Constructor<?> constructor = menuBuilderClass.getDeclaredConstructor(Context.class);
//
//	        return (Menu) constructor.newInstance(context);
//
//	    } catch (Exception e) {e.printStackTrace();}
//
//	    return null;
//	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.main, menu);
    	
    	return super.onCreateOptionsMenu(menu);
    }
    public boolean onPrepareOptionsMenu(Menu menu) {
		Log.i("TimeLogAndroidActivity", "In on prepare option");
		return super.onPrepareOptionsMenu(menu);
	}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	super.onOptionsItemSelected(item);
    	
    	switch(item.getItemId()){
	    	case R.id.gamepad:
	    		Toast.makeText(getBaseContext(), "You selected Gamepad", Toast.LENGTH_SHORT).show();
	    		break;
	    	case R.id.camera:
	    		Toast.makeText(getBaseContext(), "You selected Camera", Toast.LENGTH_SHORT).show();
	    		break;
	    	case R.id.video:
	    		Toast.makeText(getBaseContext(), "You selected Video", Toast.LENGTH_SHORT).show();
	    		break;
	    	case R.id.email:
	    		Toast.makeText(getBaseContext(), "You selected EMail", Toast.LENGTH_SHORT).show();
	    		break;
	    		
    	}
    	
    	return true;
    	
    }
	private void registerwidgets(){
		lv = (ListView)findViewById(R.id.listViewTimeLog);
		btn_add_time_log = (Button)findViewById(R.id.btnAddTimeLog);
	}
	private void setListener(){
		btn_add_time_log.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnAddTimeLog:
			Intent intent_add_time = new Intent(Activity_Time_Log.this,Activity_Add_Time_Log.class);
			Bundle bundle	=	new Bundle();
			bundle.putInt("value", 0); 
			intent_add_time.putExtras(bundle);
			startActivity(intent_add_time);
			
			break;
//		case R.id.btnDeleteTimeLog:
//			controller.deleteVolunteer();
		default:
			break;
		}
	}
	class ListAdapter1 extends ArrayAdapter<TimeEntry>{

	 	public ListAdapter1() {
	 		super(Activity_Time_Log.this, R.layout.inflate_listview_textview,volunteerList);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_listview_textview, null);
 		    }
 			TextView tv_volunteer_purpose = (TextView)convertView.findViewById(R.id.textviewInflateList);
 			TextView tv_volunteer_data = (TextView)convertView.findViewById(R.id.textviewInflateListDate);
 			Button btn_delete_volunteer = (Button)convertView.findViewById(R.id.btnDeleteTimeLog);
 			btn_delete_volunteer.setOnClickListener(new View.OnClickListener() {
 		         @Override
 		          public void onClick(View v) {
 		        	controller.deleteVolunteer(volunteerList.get(position).getId());
 		        	controller.close();
 		        	adapter.clear();
 		        	volunteerList =  controller.getAllVolunteer();
 		        	adapter.addAll(volunteerList);
 		        	adapter.notifyDataSetChanged();
// 					lv.setAdapter(adapter);

 		           }
 			});
 			tv_volunteer_purpose.setText(volunteerList.get(position).getVolunteer_purpose());
 			tv_volunteer_data.setText(volunteerList.get(position).getStartDate());
 			return convertView;	
		}
	 	

	}
	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		adapter = new ListAdapter1();
//		lv.setAdapter(adapter);
		volunteerList =  controller.getAllVolunteer();
     	adapter.clear();
     	adapter.addAll(volunteerList);
     	adapter.notifyDataSetChanged();	
//     	getOverflowMenu();

	}
//	private void getOverflowMenu() {
//
//	     try {
//	        ViewConfiguration config = ViewConfiguration.get(this);
//	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
//	        if(menuKeyField != null) {
//	            menuKeyField.setAccessible(true);
//	            menuKeyField.setBoolean(config, false);
//	        }
//	    } catch (Exception e) {
//	        e.printStackTrace();
//	    }
//	}
//	public void menuButton(){
//		ActionBar	actionBar = getActionBar();
//		actionBar.setDisplayHomeAsUpEnabled(true);
//		actionBar.setDisplayShowHomeEnabled(false);
//		actionBar.setDisplayShowTitleEnabled(true);
//		actionBar.setDisplayUseLogoEnabled(false);
//		actionBar.setDisplayShowCustomEnabled(true);
//		
//	}
}
