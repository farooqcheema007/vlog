package com.example.vloghour;

public class TimeEntry {
	String 	volunteer_id;
	String 	volunteer_purpose;
	String 	activity_information;
	String 	startDate;
	String 	endDate;
	String 	description;
	String 	hours;
	String 	supervisor;
	String 	comments;
	String 	volunteer_organization;
	String notes;
	// constructors
	public TimeEntry(){
		notes = null;
		volunteer_id = null;
		volunteer_purpose = null;
		activity_information = null;
		startDate = null;
		endDate = null;
		description = null;
		hours = null;
		supervisor = null;
		comments = null;
		volunteer_organization = null;
	}
	// setter
	public void setId(String volunteer_id) {
		this.volunteer_id = volunteer_id;
	}
	public void setvolunteerOrganization(String volunteer_organization) {
		this.volunteer_organization = volunteer_organization;
	}
	public void setHour(String hours) {
		this.hours = hours;
	}
	public void setActivity_information(String activity_information) {
		this.activity_information = activity_information;
	}
	public void setVolunteer_purpose(String setVolunteer_purpose) {
		this.volunteer_purpose = setVolunteer_purpose;
	}
	public void setStartDate(String startDate){
		this.startDate = startDate;
	}
	public void setEndDate(String endDate){
		this.endDate = endDate;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	// getter
	public String getId() {
		return this.volunteer_id;
	}
	public String getVolunteerOrganization() {
		return this.volunteer_organization;
	}
	public String getHour() {
		return this.hours;
	}
	public String getActivity_information() {
		return this.activity_information;
	}
	public String getVolunteer_purpose(){
		return this.volunteer_purpose;
	}
	public String getStartDate() {
		return this.startDate;
	}
	public String getEndDate() {
		return this.endDate;
	}
	public String getDescription() {
		return this.description;
	}
	public String getSupervisor() {
		return this.supervisor;
	}
	public String getComment() {
		return this.comments;
	}
	public String getnotes() {
		return this.notes;
	}

}
