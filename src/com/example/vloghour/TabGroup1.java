package com.example.vloghour;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;

public class TabGroup1 extends TabGroupActivity{
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        startChildActivity("", new Intent(this,Activity_Time_Log.class));
	    }
	 @Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			if ((keyCode == KeyEvent.KEYCODE_BACK)) {

				 super.onBackPressed();
				    finish();
			}
			return super.onKeyDown(keyCode, event);
		}
	  @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	    	MenuInflater inflater = getMenuInflater();
	    	inflater.inflate(R.menu.main, menu);
	    	
	    	return super.onCreateOptionsMenu(menu);
	    }
//	 @Override
//	 public boolean onCreateOptionsMenu(Menu menu)
//	 {
//	     // is activity withing a tabactivity
//	     if (getParent() != null) 
//	     {
//	         return getParent().onCreateOptionsMenu(menu);
//	     }
//	     return true;
//	 }

}
